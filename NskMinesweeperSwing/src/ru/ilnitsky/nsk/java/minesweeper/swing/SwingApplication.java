package ru.ilnitsky.nsk.java.minesweeper.swing;

import ru.ilnitsky.nsk.java.minesweeper.core.common.GameSize;
import ru.ilnitsky.nsk.java.minesweeper.core.common.ViewAutoCloseable;
import ru.ilnitsky.nsk.java.minesweeper.core.controller.Controller;
import ru.ilnitsky.nsk.java.minesweeper.core.core.MinesweeperCore;
import ru.ilnitsky.nsk.java.minesweeper.swing.swingview.FrameView;

/**
 * Программа "Сапёр" с графическим интерфейсом
 * Created by UserLabView on 21.02.17.
 */
public class SwingApplication {
    public static void main(String[] args) {

        final GameSize[] standardGameSizes = {
                new GameSize(9, 9, 10),
                new GameSize(16, 16, 40),
                new GameSize(30, 16, 99),
                new GameSize(40, 20, 99)
        };

        final String[] standardGameNames = {
                "Новичок (малый размер 9х9 ячеек, 10 мин)",
                "Любитель (средний размер 16х16 ячеек, 40 мин)",
                "Профессионал (большой размер 16х30 ячеек, 99 мин)",
                "Маньяк (очень большой размер 20х40 ячеек, 99 мин)"
        };

        try (ViewAutoCloseable view = new FrameView(30, standardGameSizes, standardGameNames)) {
            MinesweeperCore minesweeperCore = new MinesweeperCore();

            Controller controller = new Controller(minesweeperCore, view);

            view.addViewListener(controller);
            view.startApplication();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
